#!/usr/bin/node
'use strict';

var fs = require('fs-extra'),
    _ = require('underscore');

var scripts = {
  'underscore.js' : {
    prod: 'underscore/underscore-min.js',
    dev: 'underscore/underscore.js'
  },
  'underscore.string.js' : {
    prod: 'underscore.string/dist/underscore.string.min.js',
    dev: 'underscore.string/lib/underscore.string.js'
  },
  'backbone.js': {
    prod: 'backbone/backbone-min.js',
    dev: 'backbone/backbone.js'
  },
  'backbone-min.map': {
    prod: 'backbone/backbone-min.map'
  }
};

var dirs = {
  node: 'node_modules/',
  rain: 'client/js/'
};

// choose development our production sources based on NODE_ENV

var env = process.env.NODE_ENV === 'development' ? 'dev' : 'prod';

_.keys(scripts).forEach(function(dest) {
  if (scripts[dest][env]) {
    scripts[dest] = scripts[dest][env];
  } else {
    delete scripts[dest];
  }
});

scripts = _.invert(scripts);

// create target directory and copy scripts

fs.mkdirs(dirs.rain, onDirsCreated);

function onDirsCreated(e) {
  if (e) {
    handleError(e);
  } else {
    copyScripts(onScriptsCopied);
  }
}

function onScriptsCopied(e) {
  if (e) {
    handleError(e);
  } else {
    handleSuccess();
  }
}

function handleSuccess() {
  console.log('Installing the backbone component was successful.\n');
}

function handleError(e) {
  console.log('Failed to install the backbone component. Error: ' + e.message);
}

function copyScripts(cb) {
  var files = _.object(
    _.keys(scripts).map(prefix(dirs.node)),
    _.values(scripts).map(prefix(dirs.rain))
  );

  copyAll(files, cb);

  function prefix(p) {
    return function(str) {
      return p + str;
    };
  }
}

function copyAll(files, cb) {
  var sources = Object.keys(files)
    , numScripts = sources.length
    , numProcessed = 0
    , errors = [];

  _.each(sources, function(src) {
    var dest = files[src];

    fs.copy(src, dest, function(e) {
      if (e) {
        errors.push(src);
      }

      if (++numProcessed === numScripts) {
        e = null;

        if (errors.length > 0) {
          e = new Error('The following files were not copied:\n- ' + errors.join('\n- ') + '\n');
        }

        cb(e);
      }
    });
  });
}
